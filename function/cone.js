const index = require("../index"); 

const phi = 3.14;
function cone(jari, tinggi) {
  console.log((1 / 3) * phi* jari * jari * tinggi);
  return (1 / 3) * phi * jari * jari * tinggi;
}

function input() {
  index.rl.question("jari: ", (jari) => {
    index.rl.question("tinggi: ", (tinggi) => {
      if (!isNaN(jari) && !isNaN(tinggi)) {
        console.log(`\ncone: ${cone(jari, tinggi)} \n`);
        index.rl.close();
      } else {
        console.log(`jari and tinggi must be a number\n`);
        input();
      }
    });
  });
}


module.exports = { input }; 