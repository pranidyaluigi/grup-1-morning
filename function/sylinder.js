// Import readline
const index = require("../index"); // Import index to run rl on this file

// Function to calculate sylinder volume
function sylinder(jari, height) {
  const phi = 3.14;
  return phi * jari ** 2 * height;
}

/* Way 1 */
// Function for inputing Jari of Sylinder
function inputJari() {
  index.rl.question(`Jari-jari: `, (jari) => {
    if (!isNaN(jari)) {
      inputHeight(jari);
    } else {
      console.log(`You should enter the jari-jari\n`);
      inputJari();
    }
  });
}

// Function for inputing height of Sylinder
function inputHeight(jari) {
  index.rl.question(`Height: `, (height) => {
    if (!isNaN(height)) {
      inputHeight(jari, height);
    } else {
      console.log(`You should enter the height\n`);
      inputHeight(jari);
    }
  });
}

/* End Way 1 */

/* Alternative Way */
// All input just in one code
function input() {
  index.rl.question("Jari-jari: ", (jari) => {
    index.rl.question("Height: ", (height) => {
      if (!isNaN(jari) && !isNaN(height)) {
        console.log(`\nSylinder: ${sylinder(jari, height)} \n`);
        index.rl.close();
      } else {
        console.log(`Jari-jari and Height must be a number\n`);
        input();
      }
    });
  });
}
/* End Alternative Way */

module.exports = { input, inputJari }; // Export inputLength and input function, so another file can call it
