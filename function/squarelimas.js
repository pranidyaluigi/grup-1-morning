// Import readline
const index = require("../index"); // Import index to run rl on this file

// Function to calculate squareLimas volume
function squareLimas(width, lowerHeight, height) {
  return width * lowerHeight * height;
}

function inputWidth() {
  rl.question(`Please, input the width of Square Limas : `, (width) => {
    if (!isNaN(width)) {
      inputLowerHeight(width);
    } else {
      console.log(`Sorry, the data you provided was misplaced\n`);
      inputWidth();
    }
  });
}

function inputLowerHeight(width) {
  rl.question(
    `Please, input the lower height of Square Limas : `,
    (lowerHeight) => {
      if (!isNaN(lowerHeight)) {
        inputHeight(width, lowerHeight);
      } else {
        console.log(`Sorry, the data you provided was misplaced\n`);
        inputLowerLength(width);
      }
    }
  );
}

function inputHeight(lowerHeight, width) {
  rl.question(`Please, input the height of Square Limas : `, (height) => {
    if (!isNaN(height)) {
      const result = `/nSquare Limas's Volume : ${squareLimas(
        width,
        lowerHeight,
        height
      )}`;
      console.log(result);
      rl.close();
    } else {
      console.log(`Sorry, the data you provided was misplaced\n`);
      inputHeight(width, lowerHeight);
    }
  });
}

function input() {
  index.rl.question("Width: ", (width) => {
    index.rl.question("Lower Height: ", (lowerHeight) => {
      index.rl.question("Height: ", (height) => {
        if (!isNaN(width) && !isNaN(lowerHeight) && !isNaN(height)) {
          console.log(
            `\nSquare Limas: ${squareLimas(width, lowerHeight, height)} \n`
          );
          index.rl.close();
        } else {
          console.log(`Width, lower hieght, and height must be a number\n`);
          input();
        }
      });
    });
  });
}

module.exports = { inputWidth, inputLowerHeight, inputHeight, input };
