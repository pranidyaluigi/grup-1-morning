// const readline = require("readline");
// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout,
// });

const index = require("../index"); // Import index to run rl on this file

function sphere(jari) {
  const phi = 3.14;

  return 1.3 * phi * jari * jari * jari;
}

function inputJari() {
  rl.question(`Please, input jari-jari sphere : `, (jari) => {
    if (!isNaN(jari)) {
      const result = `\nSphere Volume : ${sphere(jari)}`;
      console.log(result);
      rl.close();
    } else {
      console.log(`jari-jari must be a number\n`);
      inputJari();
    }
  });
}

function input() {
  index.rl.question("Jari-jari: ", (jari) => {
    if (!isNaN(jari)) {
      console.log(`\nSphere: ${sphere(jari)} \n`);
      index.rl.close();
    } else {
      console.log(`Jari-jari must be a number\n`);
      input();
    }
  });
}

module.exports = { input, inputJari };
