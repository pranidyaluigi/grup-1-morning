// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const sylinder = require("./function/sylinder"); // import sylinder
const sphere = require("./function/sphere"); // import sphere
const cone = require("./function/cone"); // import cone
const squarelimas = require("./function/squarelimas"); // import squarelimas

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

// Function to display the menu
function menu() {
  console.log(`Menu`);
  console.log(`====`);
  console.log(`1. Sylinder`);
  console.log(`2. Sphere`);
  console.log(`3. Cone`);
  console.log(`4. Square Limas`);
  console.log(`5. Exit`);
  rl.question(`Choose option: `, (option) => {
    if (!isNaN(option)) {
      // If option is a number it will go here
      if (option == 1) {
        sylinder.input(); // It will call input() function in Sylinder file
      } else if (option == 2) {
        sphere.input(); // It will call input() function in Sphere file
      } else if (option == 3) {
        cone.input(); // It will call input() function in Cone file
      } else if (option == 4) {
        squarelimas.input(); // It will call input() function in Square Limas file
      } else if (option == 5) {
        rl.close(); // It will close the program
      } else {
        console.log(`Option must be 1 to 5!\n`);
        menu(); // If option is not 1 to 5, it will go back to the menu again
      }
    } else {
      // If option is not a number it will go here
      console.log(`Option must be number!\n`);
      menu(); // If option is not 1 to 5, it will go back to the menu again
    }
  });
}

menu(); // call the menu function to display the menu

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
